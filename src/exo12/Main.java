package exo12;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import exo11.Person;
//import exo8.Marin;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> strings = new ArrayList<String>();

		strings.add("one");
		strings.add("two");
		strings.add("three");
		strings.add("four");
		strings.add("five");
		strings.add("six");
		strings.add("seven");
		strings.add("eight");
		strings.add("nine");
		strings.add("ten");
		strings.add("eleven");
		strings.add("twelve");

		/****QUESTION 1****/
		System.out.println("/****QUESTION 1****/");
		strings.forEach(item->System.out.println(item)); //affichage du contenu de strings

		/****QUESTION 2****/
		List<String> strings2 = new ArrayList<String>();

		strings.forEach(item->strings2.add(item.substring(0,3))); //met dans strings2 les 3 premiers caract des chaines de strings
		//affichage
		System.out.println("/****QUESTION 2****/");
		strings2.forEach(item->System.out.println(item)); //affiche contenu de strings2

		/****QUESTION 3****/
		Comparator <String> cStringC = (String s1, String s2) -> s1.compareToIgnoreCase(s2);
		strings2.sort(cStringC);
		strings2.sort(Comparator.nullsLast(cStringC));

		/****QUESTION 4****/
		Map<Integer, List<String>> map1 = new HashMap<>();
		for(String s:strings) {
			map1.computeIfAbsent(s.length(), key-> new ArrayList<String>()).add(s);
		}
		//affichage
		System.out.println("/****QUESTION 4****/");
		map1.forEach((key, value) -> System.out.println(key + "->" + value));

		/****QUESTION 5****/
		Map<String, List<String>> map2 = new HashMap<>();

		for(String s:strings) {
			map2.computeIfAbsent(s.substring(0,1), key-> new ArrayList<String>()).add(s);
		}
		
		//affichage
		System.out.println("/****QUESTION 5****/");
		map2.forEach((key, value) -> System.out.println(key + "->" +value));

		/****QUESTION 6****/
		Map<String, Map<Integer, List<String>>> map3 = new HashMap<>();
		for(String s:strings) {
			map3.computeIfAbsent(s.substring(0,1), key-> new HashMap<>()).computeIfAbsent(s.length(), key-> new ArrayList<String>()).add(s);
		}
		//affichage
		System.out.println("/****QUESTION 6****/");
		map3.forEach((key, value) -> System.out.println(key + "->" +value));
		
		
		/****QUESTION 7****/
		Map<Integer, StringBuilder> map4 = new HashMap<>();  //utilisation de StringBuilder pour la concatenation

		for(String s:strings) {
			map4.computeIfAbsent(s.length(), key -> new StringBuilder()).append(s + ", ");
		}
		//affichage
		System.out.println("/****QUESTION 7****/");
		map4.forEach((key, value) -> System.out.println(key + "->" +value));
		
	}

}
